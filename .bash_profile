#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH=$PATH:/home/$USER/.config/scripts/inpath:/home/$USER/.local/bin/

# Default Applications:
export TERMINAL="xterm"
export EDITOR="nvim"
export READER="zathura"
export BROWSER="firefox"

# Convenience
export SCRIPTS="/home/$USER/.config/scripts/inpath/"
export FORPATH="/home/$USER/.config/scripts/forpath/"
export NVIMSWAP="/home/$USER/.local/share/nvim/swap/"
export COLLAGE="/mnt/hdda/collage/HNDcyberSec"
export EDU="/mnt/hdda/university/abertayY1/"

# ~/ Clean up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
export GNUPGHOME="$XDG_CONFIG_HOME"/.gnupg
export LESSHISTFILE="-"
export PYTHONSTARTUP="/home/baston/.config/.pythonrc"
export ATOM_HOME="$HOME/.config/atom"
export TRILIUM_DATA_DIR=/home/baston/gitlab/notes/trilium-data

# Start DE
if [ "$(tty)" = "/dev/tty1" ]; then
	pgrep -x i3 || exec startx
fi


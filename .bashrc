#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS2='[\! \#]\$ '
PS1="\[\033[0;32m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[1;31m\]\342\234\227\[\033[0;32m\]]\342\224\200\")[$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;0m\]root\[\033[01;33m\]@\[\033[01;96m\]\h'; else echo '\[\033[1;32m\]\u\[\033[01;36m\]@\[\033[1;32m\]\h'; fi)\[\033[0;32m\]]\342\224\200[\[\033[1;36m\]\w\[\033[0;32m\]]\n\[\033[0;32m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]\[\e[01;33m\]\\$\[\e[0m\]"

source /usr/share/doc/pkgfile/command-not-found.bash
shopt -s autocd			# Allows cd by typing the dir name
shopt -s histappend		# Append to history
HISTSIZE=""  
HISTFILESIZE=""			# Infinite history
HISTTIMEFORMAT="%F %T: "	# Adds date and time to history
HISTCONTROL=ignorespace		# Doesn't add commands starting with a " " to history
HISTFILE=~/.bash_external_history # Certain sessions truncate .bash_history
complete -cf sudo		# Allows autocomplete after sudo

# Alias general
alias ls='ls --color=auto'
alias mv="mv -iv"			# prompt before move
alias rm="rm -iv"			# primpt before removal
alias cp="cp -iv"			# Prompt before overwriting
alias c="clear"				# Shortened clear to c because I'm supper lazy
alias fk='eval "sudo $(history -p !!)"'	# Forgot to sudo say fk it
alias v="nvim "				# Lazy is bad
alias sv="sudo nvim "			# Being Lazy is bad
alias r="ranger "			# Lazy is bad
alias stopwatch="time read"		# Simple stopwatch
alias timetr="while sleep 1; do tput sc; tput cup 0 $(($(tput cols)-29));date;tput rc;done &"	# Time in the top right of the terminal
alias timesync="sudo systemctl start systemd-timesyncd.service"		# Syncs the time useing systemd
alias sudofails="sudo journalctl -e SYSLOG_FACILITY=4 -p alert"		# Shows recent sudo fails
alias more="less"       # Less is more
alias mpve="mpv --wid=$(xdotool getactivewindow) %u --ytdl-format='bestvideo[height<=?1440]+bestaudio/best'"
alias webcam="mpv av://v4l2:/dev/video0 --profile=low-latency --untimed" # "s" to take sc
alias regularUpdatesImportant="sudo pacman -Sy archlinux-keyring && sudo pacman-Su" # Forgot to update for a while? bad sigs?

# Alias fun
alias parrot="curl parrot.live"		# Shows a rainbow parrot danceing
alias weather="curl http://wttr.in/Dundee"	# Whats the weather
alias moon="curl http://wttr.in/moon"		# Moon weather

# Alias networking
alias myip='curl ipinfo.io/ip'		# Prints public IPv4 address
alias 666="ss -n -o state established '( dport = :666 or sport = :666 )'"	# These commands print all the 
alias 8999="ss -n -o state established '( dport = :8999 or sport = :8999 )'"	# active connections on the
alias 80="ss -n -o state established '( dport = :80 or sport = :80 )'"		# port specified.
alias 443="ss -n -o state established '( dport = :443 or sport = :443 )'"	#
alias sshd_logins="sudo journalctl -u sshd"	# Prints all activity within ssh
alias 2="ssh baston@192.168.1.2 -p 666"
alias 5="ssh baston@192.168.1.5 -p 666"
alias 10="ssh baston@192.168.1.10 -p 666 -i ~/.ssh/webserver_rsa"
alias 11="ssh baston@192.168.1.11 -p 666"
alias thm="sudo openvpn ~/.ssh/Baston.ovpn"	# TryHackMe VPN
alias htb="sudo openvpn ~/.ssh/hackTheBox.ovpn" # HackTheBox VPN

# Alias git
alias gita="git add -v -A"
alias gitc="git commit -v"
alias gitp="git push -4"

# Functions
# REEEEEcycling bin
trash() {
	if ! (($#)); then
		printf "You need to specify a file to trash\n"
	elif [[ "$1" == "-c" ]]; then
		rm -R $HOME/.Trash/*
	else
		mv $1 $HOME/.Trash/$1
	fi
}

# My todo list
todo() {
    if [[ ! -f $HOME/.config/todo ]]; then
        touch "$HOME/.config/todo"
    fi

    if ! (($#)); then
        nl -b a "$HOME/.config/todo"
    elif [[ "$1" == "-e" ]]; then
        $EDITOR "$HOME/.config/todo"
    elif [[ "$1" == "-r" ]]; then
        nl -b a "$HOME/.config/todo"
	printf "\n"
        read -p "Type a number to remove: " number
	if [ number ""]; then
		printf "yayeet"
	else
        	sed -i ${number}d $HOME/.config/todo "$HOME/.config/todo"
	fi
    else
        printf "%s\n" "$*" >> "$HOME/.config/todo"
    fi
}

sc() {
   old="$IFS"
   IFS='_'
   ARG="$*"
	scrot -d 2 -u "%T_$ARG.png"
   IFS=$old
}
# Show most used commands
mostused() {
	printf "Here are your most used commands\n"
	history | awk '{a[$4]++}END{for(i in a){print a[i] " " i}}' | sort -rn | head -n 40
}

colorgrid(){
    iter=16
    while [ $iter -lt 52 ]
    do
        second=$[$iter+36]
        third=$[$second+36]
        four=$[$third+36]
        five=$[$four+36]
        six=$[$five+36]
        seven=$[$six+36]
        if [ $seven -gt 250 ];then seven=$[$seven-251]; fi

        echo -en "\033[38;5;$(echo $iter)m█ "
        printf "%03d" $iter
        echo -en "   \033[38;5;$(echo $second)m█ "
        printf "%03d" $second
        echo -en "   \033[38;5;$(echo $third)m█ "
        printf "%03d" $third
        echo -en "   \033[38;5;$(echo $four)m█ "
        printf "%03d" $four
        echo -en "   \033[38;5;$(echo $five)m█ "
        printf "%03d" $five
        echo -en "   \033[38;5;$(echo $six)m█ "
        printf "%03d" $six
        echo -en "   \033[38;5;$(echo $seven)m█ "
        printf "%03d" $seven

        iter=$[$iter+1]
        printf '\r\n'
    done
}

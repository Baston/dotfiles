set ignorecase "ignore the case when searching texts.
set smartcase  "if searching text contains uppercase case will be ignored.
let mapleader=";" "set leader key
set clipboard+=unnamedplus "use clipboard
set spelllang=en_gb "set language
set nocompatible "enter current millenium
syntax enable
filetype plugin on "enable plugins

autocmd FileType txt,markdown setlocal spell spelllang=en_gb "run spellcheck on txt and markdown files
set wildmode=longest,list,full
map <F5> :set spell
map <Leader>c "*y
map <Leader>v "*p
noremap <leader>c :w \| !pdflatex $(readlink -f "<c-R>%")<CR>
noremap <leader>lw :set wrap linebreak

"aestetics
set number relativenumber "show numbers left relitive to center.
set numberwidth=2
"set colorcolumn=81 "Highlight x column.
highlight ColorColumn ctermbg=darkgray ctermfg=black "give end column a color
set laststatus=2 "always show the status bar.
set mouse=a "enables mouse support in all modes.
set numberwidth=4
set scrolloff=3
set visualbell
set tabstop=3
set expandtab
set shiftwidth=3
set softtabstop=3
set tw=79

" Function for quiet mode:
let s:hidden_all = 1
function! ToggleHiddenAll()
    if s:hidden_all  == 0
        let s:hidden_all = 1
        set nu!
        set nornu
        set noshowmode
        set noruler
        set laststatus=0
        set noshowcmd
    else
        let s:hidden_all = 0
        set nu!
        set number relativenumber
        set showmode
        set ruler
        set laststatus=2
        set showcmd
    endif
endfunction
nnoremap <leader>h :call ToggleHiddenAll()<CR>

call plug#begin('~/.config/nvim/plugged')
Plug '~/.config/nvim/plugged/swapcol.vim' "swapcol.vim
call plug#end()

highlight SpellBad	ctermfg=Black	ctermbg=Red
highlight SpellCap	ctermfg=Red	ctermbg=Black
highlight SpellLocal	ctermfg=Cyan 	ctermbg=Black
highlight SpellRare	ctermfg=Magenta	ctermbg=Black

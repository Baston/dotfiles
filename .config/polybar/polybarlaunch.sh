#!/bin/bash

# Kill alreddy running instance
killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch polybar
polybar mybar &

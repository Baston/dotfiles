#!/usr/bin/env python3
import time
from multiprocessing import Process
from socket import socket, AF_INET, SOCK_STREAM
import sys
from time import sleep
from datetime import datetime

def main():
    port_start = 1
    port_end = 443
    port_end = 999

    _, port_start, port_end = sys.argv

    processes = []
    for port in range(int(port_start), int(port_end) + int(1)):
        sleep(0.1)
        process = Process(target=test_port, args=(port,))
        process.start()
        processes.append(process)

    # while len(processes) > 0:
    #     print('\t' + str(len(processes)), end="\r")
    #     for process in processes:
    #         if not process.is_alive():
    #             processes.remove(process)


def test_port(port):
    sock = socket(AF_INET, SOCK_STREAM)
    result = sock.connect_ex(("blog.baston.uk", int(port)))
    if result == 0:
        print("Port {}: is open".format(port))
    sock.close()

if __name__ == '__main__':
    try:
        t1 = datetime.now()
        main()
        t2 = datetime.now()
        total = t2 - t1
        print("Scan compleated in: ", total)
    except KeyboardInterrupt:
        sys.exit(0)

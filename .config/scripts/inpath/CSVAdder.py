#!/bin/env python3
import codecs

loc = input("Where's the CSV: ")

file = codecs.open(loc, "r", 'ascii')

line = file.read()
g = line.split(",")
print(g)

total = sum([float(num) for num in g])

print(total)

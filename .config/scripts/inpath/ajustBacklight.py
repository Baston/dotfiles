#!/bin/python3
# Script to ajust brightness on my laptop
# By Callum Baston
# 2020

import sys
import subprocess

# File where brightness is
brightness_file = open("/sys/class/backlight/intel_backlight/brightness", "r+")
current_brightness = brightness_file.read()

counter = 0
location = 7

# Array of possible brightnesses
brightnesses = ['2060', '3100', '4650', '6980', '10470', '15700', '23600', '35300', '53000', '80000', '110000']

# Remove the \n from the end
current_brightness = current_brightness[:-1]

# find the location of the current brightness
try:
    location = brightnesses.index(current_brightness)
except:
    pass

# Check if there are too many or little arguments passed
if len(sys.argv) == 1:
    brightness_file.close()
    sys.exit("Must add an argument")
elif len(sys.argv) >= 3:
    brightness_file.close()
    sys.exit("Too many arguments")

# change value based on the argument passed
try:
    arg = int(sys.argv[1])
    if arg <= 119999 and arg >= 50:
        end_brightness = arg
    else:
        sys.exit("")
except:
    if sys.argv[1].lower() == "up":
        location = location + 1
    elif sys.argv[1].lower() == "down":
        if location > 0:
            location = location - 1
    elif sys.argv[1].lower() == "default":
        location = 5
    else:
        sys.exit("The options are \"up\", \"down\", \"default\" or an int between 50 and 120000")
    end_brightness = brightnesses[location]

# Set and write the final brightness
end_brightness = str(end_brightness)
brightness_file.write(end_brightness)
brightness_file.close()
# Display a notfiaction using notift-send
subprocess.call(["notify-send", end_brightness, "-a", "brightness", "-u", "low", "-t", "1000"])


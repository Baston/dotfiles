#!/bin/bash

# This is a backup script designed to be ran from my machines.
# It is not designed to be ran from YOUR machine. Please don't
# run this on your computer.

#set -u  # Exit if hits an unset varible
set -e  # Exit if anything returns an error

# This controlls what happens when an interupt is passed
# e.g. Ctrl+C is passed or exit from ssh while scipt is running
int_handler()
{
   echo "Script interupted, there has been no cleanup"
   kill $PPID
   exit 1
}
trap 'int_handler' INT

# Checks current script privileges
function check_priv(){
	if [[ $(id -u) -ne 0 ]]; then
		echo "This part of the script must be ran as root"
		echo "sudo is reccomended"
      exit 1
	else
		echo "Running as $USER"
	fi
}


# Backup script for the pavilion computer.
# Use the drive opening script to get /dev/disk/by-uuid
function pavilion(){
   check_priv
	echo "AND DESIGNED TO BE RAN FROM A LIVE Arch/Debian OS"
	printf "\n"
	echo "Do you really want to run this script? YES/n"
	read -r CONTINUE
	if [ "$CONTINUE" != YES ]; then
		echo "You have desided not to run the backup script"
		exit 1
	fi
	echo "Now preforming pavilion backup"
	mkdir /mnt/hdda
	mkdir /mnt/nvme
	cryptsetup open /dev/disk/by-uuid/330f99a0-af11-462a-948c-4bb2d74cc071 nvme
	mount /dev/mapper/nvme /mnt/nvme
	lsblk
	cryptsetup open /dev/disk/by-uuid/a5fc207e-ba0d-4a5e-9d79-e764b9991c7a hdda --key-file=/mnt/nvme/root/keyfiles/5tbhdd
	mount /dev/mapper/hdda /mnt/hdda
	umount /mnt/nvme
	cryptsetup close nvme
	rmdir /mnt/nvme
	sleep 1
	lsblk -f
	echo "Please check that everything has been done correctly before you run the DD commands. Have you checked everything? [Y/n] "
	
   read -r CONTINUE
	if [ "$CONTINUE" = "Y" ]; then
		dd if=/dev/nvme0n1p5 of=/mnt/hdda/backups/pavilion/"$(date +%F)"-nvme0n1p5.img status=progress
		dd if=/dev/nvme0n1p6 of=/mnt/hdda/backups/pavilion/"$(date +%F)"-nvme0n1p6.img status=progress
    	chown baston:baston /mnt/hdda/backups/pavilion/*
	else
		echo "You did not enter \"Y\""
		echo "Here are the DD commands for you to do yourself remember to tar, unmount and close devices afterwards."
		echo "dd if=/dev/nvme0n1p5 of=/mnt/hdda/backups/pavilion/$(date +%F)-nvme0n1p5.img status=progress"
		echo "dd if=/dev/nvme0n1p6 of=/mnt/hdda/backups/pavilion/$(date +%F)-nvme0n1p6.img status=progress"
	fi
   
   echo "Do you want to create the tar archive now? Y/n"
   read -r CONTINUE
   if [ "$CONTINUE" = "Y" ]; then 
      tar -czvf /mnt/hdda/backups/pavilion/"$(date +%F)"-nvme0n1p5+6.tar.gz /mnt/hdda/backups/pavilion/"$(date +%F)"-nvme0*

   umount /mnt/hdda
   cryptsetup close hdda
   rmdir /mnt/hdda
	echo "Backup Complete"
   fi
}

# Backup for the main PC
function main(){
	echo "Why the hell arn't you using clonezilla???"
}

# Backup USB drive
function usb(){
   check_priv
	echo "Select the USB drive you want to back up, e.g. sda1"
	lsblk
	read -r DRIVE
	echo "Enter backup destanation"
	read -r DESTANATION
	echo "What's the drive backup name?"
	read -r DRIVENAME
	dd if=/dev/"$DRIVE" of="$DESTANATION"/"$(date +%F)"-"$DRIVENAME".img status=progress
	echo "Backup complete"
}

# Backup Collage data
function collage(){
	echo "Backing up $COLLAGE directory"
	COLLAGE_FILE=HNDCyberSec"$(date +%F)".tar.gz
	tar -czvf "$COLLAGE_FILE" "$COLLAGE"
	gpg --symmetric --cipher-algo AES256 -vv "$COLLAGE_FILE"
   rm "$COLLAGE_FILE"
   echo "Backup complete and encrypted. Please upload to cloud"
}

# Backup University work
function uni(){
   echo "Backing up /mnt/hdda/university/ dir"
   UNI_FILE=AbertayUni"$(date +%F)".tar.gz
   tar -czv --exclude=*ova --exclude=*ova.bak -f "$UNI_FILE" /mnt/hdda/university/
   gpg --symmetric --cipher-algo AES256 -vv "$UNI_FILE"
   rm "$UNI_FILE"
   echo "Backup complete and encrypted. Please upload to cloud"
}

# Backup wordpress data
function wpBackup(){
   tar -cvzf /home/baston/server1-$(date +%F).tar.gz /home/baston/storage/*
   # scp -P 666 baston@baston.uk@server1-$(date +%F).tar.gz
   # /mnt/hdda/backups/Lab/cloudOne/backups
}

# LAN backup to NAS device based on current devices hostname
function nas(){
   HOST=$(hostname)
   if [[ "$HOST" == "" ]]; then
      echo "hostname broke"

   elif [[ "$HOST" == "pavilion" ]]; then
      if ping -4 -c 3 -i 0.5 -I eno1 -W 10 192.168.1.5
      then
         mount -a
         rsync -avzh --stats -e 'ssh -p 666' --progress \
            /mnt/hdda/backups \
            /mnt/hdda/Downloads/Photos \
            /mnt/hdda/Downloads/Other \
            /mnt/hdda/Downloads/Music \
            /mnt/hdda/Downloads/Files \
            /mnt/hdda/motarLearning \
            /mnt/hdda/collage \
            /mnt/hdda/university \
            /mnt/virtualBox \
            baston@192.168.1.5::NetBackup
            #/home/baston/Documents \
         notify-send -t 0 -a "rsync" "SynNAS" "Backup Complete"
      else
         echo "Cannot Ping SynNAS"
         notify-send -t 0 -a "rsync - FAIL" "SynNAS" "Unable to ping SynNAS, is
         WOL enabled?"
      fi

   elif [[ "$HOST" == "R402" ]]; then
      if [[ ! -f /tmp/backups ]]; then
         mkdir /tmp/backups
      fi

      BackupFile="R402-home$(date +%F).tar.gz"
      tar --exclude=git -cvzf /tmp/backups/"$BackupFile" /home/baston
      sshpass -p $(cat /root/.ssh/rsync) rsync -avzh --preallocate --stats \
         -e 'ssh -p 666' --progress \
         /tmp/backups \
         R402@192.168.1.5::home/
      rm /tmp/backups/"$BackupFile"

   else
      echo "Host not recognised"
   fi
}

# Quick prevision for backup security
function secure(){
   check_priv
	df -h
	if ping -4 -c 1 -w 10 archlinux.org; then
		echo "Disconnecting you from the network..."
		networkctl down eno1 || networkctl down eth0 || networkctl down wlan0 || networkctl down enp0s1 || echo "Couldn't disable network?"
	else
		echo "Internet connection down ERROR:$?"
	fi
	echo "I bet you forgot the commands again, dumbass"
	if ! command -v cryptsetup &> /dev/null; then
		echo "!!! cryptsetup not found !!!"
		exit 1
	else
		cryptsetup -V
	fi

}

# Main backup function
function backup(){
	echo "Warning this may damage your install as it is unfinished"
	NET=1
   BACKUP_MODE=""
   BACKUP_MODE=$1
   echo $BACKUP_MODE
   if [[ -z "$BACKUP_MODE" ]]; then
	   echo "What machine Will you be backing up today"
	   echo "\"Pavilion\", \"Main\", \"USB\", \"Collage\", \"Secure\", \"Uni\",
      \"nas\", \"wpBackup\" "
	   read -r BACKUP_MODE
   fi
   echo "Current time $(date +%F/%X)"
	case "$BACKUP_MODE" in
      pav | Pavilion | pavilion ) echo "You have selected $BACKUP_MODE"; pavilion ;;
      mai | Main | main ) echo "You have selected $BACKUP_MODE"; main ;;
      usb | Usb | USB ) echo "You have selected $BACKUP_MODE"; usb ;;
      col | collage | Collage ) echo "You have selected $BACKUP_MODE"; collage;;
      uni | university | Uni ) echo "You have selected $BACKUP_MODE"; uni ;;
      wpb | wordpress | wpBackup ) echo "You have selected $BACKUP_MODE"; wpBackup ;;
      nas | remote | synnas ) echo "You have selected $BACKUP_MODE"; nas ;;
      sec | secure | Secure ) echo "Now securing system"; secure ;;
		'') echo "you didn't select anything?"; exit 1 ;;
	esac
	if [ $NET = 0 ]; then
		networkctl up eno1 || networkctl up eth0 || networkctl up wlan0 || networkctl up enp0s1 || echo "Couldn't enable networking?"
	else
		echo "All done!"
	fi
}

# Run the main function
backup $1

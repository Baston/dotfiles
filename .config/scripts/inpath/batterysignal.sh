#!/bin/bash
Sleep=180
while true
do
	Bat=$(acpi | head -n 1 | cut -d " " -f 4 | tr -d % | tr -d ,)
	sleep $Sleep
	if [ "$Bat" -gt 79 ]; then
		notify-send -u low -t 10000 -a "Battery Level" "Battery is above 80%" "Level: $Bat%"
	elif [ "$Bat" -lt 21 ] && [ "$Bat" -gt 10 ]; then
		notify-send -a "Battery Level" "Battery is below 20%" "LeveL: $Bat%"
		Sleep=180
	elif [ "$Bat" -lt 11 ]; then
		notify-send -u critical -a "Battery Level" "Battery is below 10%" "Level: $Bat%"
		canberra-gtk-play -i audio-volume-change
		Sleep=60
	fi
done

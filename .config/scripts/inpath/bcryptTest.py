#!/bin/python
# -*- coding: utf-8 -*-

import bcrypt
import time
from pathlib import Path
from hashlib import sha256
import subprocess
import uuid

def test(rounding, fileLocation):
    password = "This_is_an_example_of_a_P@ssw0rd_that_is_very_long_that_a_User_Might_use_It_has_all_qwerty_keys_`1234567890-=¬!\"£$%^&*()_+qwertyuiop[]#QWERTYUIOP{}~asdfghjkl;'ASDFGHJKL:@zxcvbnm,./ZXCVBNM<>?\|_And_Some_extras_|¹²³€½¾{[]}\łe¶ŧ←↓→øþßðđŋħł»¢“”nµ,·_evenMore?_¡⅛£¼⅜⅝⅞™±°¿Øı↑¥Ŧ®EŁΩÆ§ÐªŊĦŁ×ºN’‘©><_300_chars".encode("utf-8")
    start = time.time()
    
    for i in range(10):
        hashed_password = bcrypt.hashpw(bytes(sha256(
            password).hexdigest(), 'utf-8'), bcrypt.gensalt(rounds=rounding)).decode('utf-8')
        
    end = time.time()
    t = end - start
    print("time for " + str(rounding) + " rounds")
    print(t)
    with open(fileLocation, "a") as f:
        f.write("Rounds: " + str(rounding) + ", time per run: " + str( t / 10) + ", Total time: " + str(t) + "\n" )


def start():
    rounds = 4
    print("Starting bcrypt rounds test from 4 to 17 rounds")
    print("Data is in your home directory")
    print("This script records your computers processor information.")
    fileLocation = str(Path.home()) + "/bcryptHashSpeed" + str(uuid.uuid4()) + ".txt"
    for i in range(14):
        test(rounds, fileLocation)
        rounds = rounds + 1 
    with open(fileLocation, "a") as f:
        f.write(str((subprocess.check_output("lscpu", shell=True).strip()).decode()))

start()

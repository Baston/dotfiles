#!/bin/bash


notify-send -t 20000 -a "CPU Info" " " "$(grep "MHz" /proc/cpuinfo) \n $(sensors | sed '17,23!d' | sed -r 's/.{36}$//')"

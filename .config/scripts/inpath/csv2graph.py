#!/bin/env python3

import pandas as pd
import plotly.express as px
from sys import argv

df = pd.read_csv(argv[1])

fig = px.line(df, x = 'Date', y = 'Temp', title= 'Temp to time')
fig.show()


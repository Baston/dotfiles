#!/bin/bash

FILES=${1:-"/home/baston/.config/screenlayout"}
DMENU='dmenu -i'

choice=$(ls ${FILES} | $DMENU -p "Output Mode: ")

if [ $choice ]; then
   /bin/bash $FILES/$choice
fi

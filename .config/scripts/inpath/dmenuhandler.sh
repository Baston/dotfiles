#!/bin/bash
case "$(printf "w3m\\numpv\\ncopy url\\nbrowser\\ntor" | dmenu -i -p "Open link with what program?")" in
   w3m) w3m $1 ;;
   umpv) umpv.py "$1" >/dev/null 2>&1 ;;
   "copy url") echo "$1" | xclip -selection clipboard ;;
   browser) setsid -f "$BROWSER" "$1" >/dev/null 2>&1 ;;
   tor) torbrowser-launcher "$1" >/dev/null 2>&1 ;;
   "mpv (float)") setsid -f mpv --geometry=+0-0 --autofit=30%  --title="mpvfloat" "$1" >/dev/null 2>&1 ;;
esac

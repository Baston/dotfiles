#!/bin/bash

OUTPUT_FILE=$3

if [[ "$1" == "-ns" ]]; then
   echo "nvidia small option"

elif [[ "$1" == "-nh" ]]; then
   echo "nvidia heigh quality option"

elif [[ "$1" == "-nn" ]]; then
   echo "Nvidia normal option"
   ffmpeg -hwaccel cuvid -i $2 -c:v hevc_nvenc -rc:v vbr -cq:v 28 $OUTPUT_FILE

elif [[ "$1" == "-cn" ]]; then
   echo "Normal CPU option"
   ffmpeg -i $2 -vcodec libx265 -crf 28 $OUTPUT_FILE

elif [[ "$1" == "-cs" ]]; then
   echo "Slow CPU option"
   ffmpeg -i $2 -vcodec libx265 -preset verysloq -crf 28 -c:a copy $OUTPUT_FILE

else
   echo "The options are: "
   echo "-nn Nvidia normal option"
   echo "-cn Normal CPU encoding"
   echo "-cs Slow CPU encoding"

fi

#!/bin/bash
# Thing to do a thing
# by Callum Baston

INTERFACE=$(ip a | grep " UP " | cut -d ' ' -f 2 | tail -n 1)
INTERFACE=${INTERFACE::-1}

RX_BYTES=$(cat /sys/class/net/$INTERFACE/statistics/rx_bytes)
TX_BYTES=$(cat /sys/class/net/$INTERFACE/statistics/tx_bytes)

echo "RX $((RX_BYTES / 1024 / 1024))MB TX $((TX_BYTES / 1024 / 1024))MB"

#/bin/bash

find . -maxdepth 1 -name "*.jpg" -exec mogrify -format PNG {} \;

#!/bin/bash


## MOUSE OPTIONS

#Touchpad ELAN0710:01 04F3:30ED Touchpad
xinput set-prop 'ELAN0710:01 04F3:30ED Touchpad' 'libinput Tapping Enabled' 1 
xinput set-prop 'ELAN0710:01 04F3:30ED Touchpad' 'libinput Accel Speed' 0.1 
xinput set-prop 'ELAN0710:01 04F3:30ED Touchpad' 'libinput Disable While Typing Enabled' 1 

# MLG Sensi
xinput --set-prop 'La-VIEW Technology SteelSeries Sensei MLG' 'libinput Accel Speed' -1

# HP Pavilion mouse
xinput --set-prop 'HP HP Pavilion Gaming Mouse 300' 'libinput Accel Speed' -1

# ROCCAT Kova mouse
xinput --set-prop 'ROCCAT ROCCAT Kova Mouse' 'libinput Accel Speed' -1

# ET mouse
xinput --set-prop pointer:'MOSART Semi. 2.4G Keyboard Mouse' 'libinput Accel Speed' -0.75

# imwheel stuff

cat >~/.imwheelrc<<EOF
".*"
None,      Up,   Button4, 3
None,      Down, Button5, 3
Control_L, Up,   Control_L|Button4
Control_L, Down, Control_L|Button5
Shift_L,   Up,   Shift_L|Button4
Shift_L,   Down, Shift_L|Button5
EOF

# imwheel -k -q
xinput --list-props 'La-VIEW Technology SteelSeries Sensei MLG' || cat >~/.imwheelrc<<EOF
".*"
None,      Up,   Button4, 1
None,      Down, Button5, 1
Control_L, Up,   Control_L|Button4
Control_L, Down, Control_L|Button5
Shift_L,   Up,   Shift_L|Button4
Shift_L,   Down, Shift_L|Button5
EOF


cat ~/.imwheelrc
imwheel

## KB OPTIONS

# set kb latout and swap caps and escape
setxkbmap gb -option caps:swapescape
# speed up inputs after delay
xset r rate 300 75


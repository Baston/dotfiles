#!/bin/bash

# Approx timeout rate in ms (checked every second)
timeout="5000"

# Take screenshot
#scrot -o /tmp/lock_screen.png

# Start i3lock
#i3lock -c 333333

#xset dpms force off

# Swirl and blur the image then add the lock
#[[ -f ~/.config/i3/lock.png ]] && convert /tmp/lock_screen.png -paint 7 ~/.config/i3/lock.png -gravity center -composite -matte /tmp/lock_screen.png

# Close the locker and reopen it with the new image
#pkill i3lock && i3lock -i /tmp/lock_screen.png -c 000000 -e -f

# New system from wallpapers
i3lock -i $(printf "%s%s" "$HOME/.config/wallpapers/single/" "$(ls $HOME/.config/wallpapers/single/ | shuf -n 1)") -c 000000 -e -f

xset dmps force off

# If still locked after timeout ms turn off screen
while [[ $(pgrep -x i3lock) ]]; do
	[[ $timeout -lt $(xssstate -i) ]] && xset dpms force off
	sleep 1
done

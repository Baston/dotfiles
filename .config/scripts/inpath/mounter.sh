#!/bin/bash

# Drive mounting script. 
# Callum Baston

set -e  # Exit if anything returns an error

# This controlls what happens when an interupt is passed
# e.g. Ctrl+C is passed or exit from ssh while scipt is running
int_handler()
{
   echo "Script interupted, there has been no cleanup"
   kill $PPID
   exit 1
}
trap 'int_handler' INT

# Checks current script privileges
function check_priv(){
	if [[ $(id -u) -ne 0 ]]; then
		echo "This part of the script must be ran as root"
		echo "sudo is reccomended"
      exit 1
	else
		echo "Running as $USER"
	fi
}

# Temp mount secure USB drive
function secureUSB(){
   sudo cryptsetup open /dev/disk/by-uuid/d510c062-7216-4fb9-b709-7917dcc633be usb
   sudo mount /dev/mapper/usb /mnt/usb
   sleep 1
   echo "Press RTN to close and umount drive"
   read NULL

   sudo umount /mnt/usb
   sudo cryptsetup close usb
}

function usbCheck(){
  echo "not doen yer" 
}

function main(){
   check_priv
   MODE=$1
   echo $MODE
   if [[ -z "$MODE" ]]; then
      echo "what mode: "
	   read -r MODE
   fi

	case "$MODE" in
      --check | check | chk ) usbCheck ;;
      --mount | mount | mnt ) mountUsbs ;;
      
      --secureUSB | secureUSB | secusb ) secureUSB ;;
      sec | secure | Secure ) echo "Now securing system"; echo "ooft" ;;
      *) echo "$1 isn't a valid choice"; exit 1 ;;
		'') echo "you didn't select anyoptiond?"; exit 1 ;;
	esac
}

# Run the main function
main $1

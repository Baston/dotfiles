#!/bin/bash

HOST=$(hostname)

if [[ "$HOST" == "" ]]; then
   echo "No host specified"

elif [[ "$HOST" == "pavilion" ]] ; then
   if ping -W 10 -c 2 192.168.1.5
   then 
      mount -a
      rsync -avh --stats -e 'ssh -p 666' --progress \
         /mnt/hdda/backups \
         /mnt/hdda/Downloads/Photos \
         /mnt/hdda/Downloads/Other \
         /mnt/hdda/Downloads/Music \
         /mnt/hdda/Downloads/Files \
         /mnt/hdda/motarLearning \
         /mnt/hdda/collage \
         /mnt/hdda/university \
         /home/baston/Documents \
         baston@192.168.1.5::NetBackup
      notify-send -t 0 -a "rsync" "SynNAS" "Bacup successfull"
   else 
      echo "CANNOT PING DEVICE"
      notify-send -t 0 -a "rsync FAILED" "SynNAS" "Bacup FAULED"
   fi

elif [[ "$HOST" == "R402" ]]; then
   sudo rsync -avh --stats -e 'ssh -p 666' --progress \
      /home/baston/storage \
      /home/baston/docker-compose.yml \
      R402@192.168.1.5::home/
else
   echo "Host not recognised"
fi


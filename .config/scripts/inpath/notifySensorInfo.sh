#!/bin/bash

if [[ "$1" == "-c" ]]; then
	notify-send -t 20000 -a "CPU Info" " " "$(grep "MHz" /proc/cpuinfo) \n$(sensors | awk '/^Core/' | sed -r 's/.{36}$//')"

elif [[ "$1" == "-C" ]]; then
	notify-send -t 20000 -a "CPU Usage" " " "$(vmstat | sed -r 's/.{65}//' | sed -r 's/.[0-9][0-9]//')\n$(ps axch -o cmd:20,%cpu --sort=-%cpu | sed 11q)"

elif [[ "$1" == "-Ca" ]]; then
	notify-send -t 20000 -a "CPU Usage" " " "$(vmstat | sed -r 's/.{65}//' | sed -r 's/.[0-9][0-9]//')\n$(ps axh -o cmd:36,%cpu --sort=-%cpu | sed 21q)"

elif [[ "$1" == "-m" ]]; then
	notify-send -t 20000 -a "Memory Usage" " " "$(free -h | awk '/^Mem:/{ print $3 " / " $2}') $(echo "\n")$(ps axch -o cmd:20,%mem --sort=-%mem | sed 11q)"

elif [[ "$1" == "-t" ]]; then
	notify-send -t 20000 -a "Tempratures" " " "$(sensors | sed -r 's/.{36}$//')"
else
	printf "Options are: \"-c\" \"-m\" \"-t\", \n"
	printf "All information is passed using notify-send \n"
	printf "\n"
	printf " -c   Sends current CPU speed and temp \n"
	printf " -C   Sends CPU usage along with 10 most intensive processes \n"
	printf " -Ca  Sends CPU usage with 10 most intensice processes with full path names \n"
	printf " -m   Sends Memory usage and 10 most intensive processes \n"
	printf " -t   Sends currnet temprature of devices \n"
	printf "\n"
	printf "4 min to go"
fi

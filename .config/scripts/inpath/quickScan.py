#!/usr/bin python3

import sys
import socket 
import re
from ipaddress import *
import threading
from queue import Queue


threads = 100
queue = Queue()
print_lock = threading.Lock()


def get_targets():
    target = sys.argv[1]
    while re.match("([0-9]{1,3}.){3}[0-9]{1,3}$", target) is None:
        print("Target is not a valid IP address")
        exit(1)
    target = ip_address(target)
    return target

def get_ports():
    ports = sys.argv[3]
    
    if ports == str(1):
        print("scanning common ports")
        ports = range(1,1025)
        return ports
    elif ports == str(2):
        print("scanning all ports")#
        ports = range(1, 65536)
        return ports
    elif len(ports) >= 2:
        lst =[]
        ports = ports.split(",")
        print(ports)
        for i in ports:
            lst.append(int(i))
        return lst
    else:
        print("determining up using port 7")
        return "7"

def scan(i, target):
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = skt.connect_ex((target, i))
    if result == 0:
        print("[+]tcp %s is open on host %s" % (i, target))
        openPorts.append(i)
        skt.close()
    else:
        print("[-]tcp %s is closed on host %s" % (i, target))
        closedPorts.append(i)

def scan_thread():
    global queue
    while True:
        worker = queue.get()
        scan(worker, target)
        queue.task_done()

def start():
    global closedPorts
    closedPorts =[]
    global openPorts
    openPorts =[]
    socket.setdefaulttimeout(.01)
    global target
    target = str(get_targets())
    ports = get_ports()
    try:
        for t in range(threads):
            t = threading.Thread(target=scan_thread)
            t.daemon = True
            t.start()

        for worker in ports:
                queue.put(worker)
                #scan(i, target)
        queue.join()

        print("summery")
        print("Open Ports: ", openPorts)
    except socket.error as error:
        pass


if len(sys.argv) < 2:
    print("usage:")
    print("quckScan.py <ip> <netmask> <ports>")
    print(" netmask can be anything cus it's not done yet")
else:
    start()

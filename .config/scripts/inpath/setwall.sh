#!/bin/bash

BGFOLDER="$HOME/.config/wallpapers/single/"
BGFOLDER2="$HOME/.config/wallpapers/double/"

case "$1" in
	-c*) xwallpaper --zoom $(printf "%s%s" "$BGFOLDER" "$(ls $BGFOLDER | dmenu -l 50)") ;;
	*) xwallpaper --zoom $(printf "%s%s" "$BGFOLDER" "$(ls $BGFOLDER | shuf -n 1)") ;;
   -dc*) xwallpaper --zoom $(printf "%s%s" "$BGFOLDER2" "$(ls $BGFOLSER2 | dmenu -l 15)") ;;
	-d*) xwallpaper --zoom $(printf "%s%s" "$BGFOLDER2" "$(ls $BGFOLDER2 | shuf -n 1)") ;;
esac

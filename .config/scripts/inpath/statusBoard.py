#!/bin/python3

from gpiozero import StatusBoard, CPUTemperature, PingServer
from time import sleep
import datetime
import requests
import speedtest
import threading
from http.client import HTTPConnection

sb = StatusBoard('website', 'lanNetwork', 'nas', 'R2TMP', 'nasavanys', pwm=True)

def allOff():
    sb.website.lights.value = (0, 0)
    sb.nas.lights.value = (0, 0)
    sb.R2TMP.lights.value = (0, 0)
    sb.lanNetwork.lights.value = (0, 0)
    sb.nasavanys.lights.value = (0, 0)


def speedTest():
    allOff()
    servers = []
    threads = None
    s = speedtest.Speedtest()
    s.get_best_server()
    down = s.download(threads=threads)
    up = s.upload(threads=threads)
    #s.results.share()
    results_dict = s.results.dict()

    if down > 90000000:  # 90Mb
        sb.website.lights.green.on()
    if down > 70000000:  # 70Mb
        sb.lanNetwork.lights.green.on()
    if down > 50000000:  # 50Mb
        sb.nas.lights.green.on()
    if down > 30000000:  # 30Mb
        sb.R2TMP.lights.green.on()
    if down > 10000000:  # 10Mb
        sb.nasavanys.lights.green.on()

    if up > 180000000:  # 18Mb
        sb.website.lights.red.on()
    if up > 16000000:  # 16Mb
        sb.lanNetwork.lights.red.on()
    if up > 14000000:  # 14Mb
        sb.nas.lights.red.on()
    if up > 12000000:  # 12Mb
        sb.R2TMP.lights.red.on()
    if up > 10000000:  # 10Mb
        sb.nasavanys.lights.red.on()

    print("Upload: " + str(up) + " Download: " + str(down))
    print("Upload: " + str((up / 1000000)) + "Mb Download: " + str((down / 1000000)) + "Mb")
    logger("speedTest", "Upload: " + str(up) + " Download: " + str(down))
    logger("speedTest", "Upload: " + str((up / 1000000)) + "Mb Download: " + str((down / 1000000)) + "Mb")

def fullTime():
    now = datetime.datetime.now()
    return str(now.strftime('%Y-%m-%d_%H:%M:%S.%f'))

def connectTest(host):
    try:
        conn = HTTPConnection(host, '80', 3)
        conn.request("HEAD", "/")
        conn.close()
        return True
    except Exception as e:
        return e

def nas_module():
    if website_up('http://192.168.1.5:5050/') == True:
        if website_up('http://jelly.lan/web/index.html') == True:
            sb.nas.lights.green.value = 0.1
            sb.nas.lights.red.value = 0
            status = "All-Okay"
        else:
            sb.nas.lights.green.value = 0.5
            sb.nas.lights.red.value = 0.5
            status = "jelly-down"
    else:
        sb.nas.lights.red.value = 0.05
        status = "NAS-Offline"

    logger("nas_module", 'Checking NAS status. Result: ' + status)


def website_up(url):
    try:
        r = requests.head(url)
        return r.status_code == 200
    except:
        return False

def website_module():
    if connectTest('www.baston.uk') == True:
        if website_up('https://baston.uk/') == True:
            sb.website.lights.green.value = 0.01
            sb.website.lights.red.value = 0
            status = "All-Online"
        else:
            sb.website.lights.green.value = 0
            sb.website.lights.red.value = 0.05
            status = "Website-Down"
    else:
        sb.website.lights.blink()
        status = "WAN-Down"

    logger("website_module", 'Doing internet test. Result: ' + status)


def R402_Temp():
    cpu_temp = CPUTemperature().temperature
    if cpu_temp < 60:
        sb.R2TMP.lights.green.value = 0.1
        sb.R2TMP.lights.red.value = 0
    elif cpu_temp > 60 and cpu_temp < 70:
        sb.R2TMP.lights.green.value = 0
        sb.R2TMP.lights.red.value = 0.1
    elif cpu_temp > 69:
        sb.R2TMP.lights.green.value = 0
        sb.R2TMP.lights.red.blink()
    else:
        sb.R2TMP.lights.blink()
    
    logger("R402_Temp", "Temperature of R402. Result: " + str(cpu_temp) + '°C')

def isOnNetwork():
    router = PingServer('192.168.1.254')
    dns = PingServer('192.168.1.2')
    if router.is_active:
        if dns.is_active:
            sb.lanNetwork.lights.green.value = 0.1
            sb.lanNetwork.lights.red.value = 0
            status = "LAN-Okay"
        else:
            sb.lanNetwork.lights.green.value = 0
            sb.lanNetwork.lights.red.value = 0.01
            status = "DNS-Down"
    else:
        sb.lanNetwork.lights.blink()
        status = "Router-Down"
    logger("isOnNetwork", "Checking core network infrastructure. Result: "  + status)


def onClickListener():
    sb.website.button.when_pressed = speedTest
    sb.lanNetwork.button.when_pressed =  sb.lanNetwork.lights.pulse
    sb.nas.button.when_pressed = sb.nas.lights.pulse
    sb.R2TMP.button.when_pressed = allOff
    sb.nasavanys.button.when_pressed = allOff


def logger(function, info):
    logFile = open('status.log', 'a')
    logFile.write(fullTime() + " " + function + ": " + info + "\n")
    logFile.close()


def main():
    onClickListener()
    counter = 0
    logger("main", "Application started")
    # 3600 seconds in a hour
    while True:

        if not counter % 600:
            thrd1 = threading.Thread(target=website_module)
            thrd1.start()
    
        if not counter % 10:
            thrd2 = threading.Thread(target=isOnNetwork)
            thrd2.start()

        if not (counter + 1) % 10:
            thrd3 = threading.Thread(target=nas_module)
            thrd3.start()
    
        if not (counter + 2) % 10:
            thrd4 = threading.Thread(target=R402_Temp)
            thrd4.start()

        sleep(1)
        counter += 1
        if counter == 3600:
            counter = 0


if __name__ == "__main__":
    main()

#!/bin/bash

if [[ "$1" == "-scratch" ]]; then
	xterm -ls -name scratch
elif [[ "$1" == "-p" ]]; then
	xterm -fa DejaVumono -fs 11 -ls -xrm 'XTerm*selectToClipboard: true' -e "$2 $3"
else
	xterm
fi

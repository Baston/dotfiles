#!/bin/python3

from datetime import datetime
from sys import argv

unix_timestamp = int(argv[1]) / 1000 /1000

date_obj = datetime.fromtimestamp(int(unix_timestamp))

print("Datetime: " + str(date_obj))

# Contributing
Thank you for considering improving my dotfiles. If you wish to improve them
just submit a pull request and I'll merge your suggestion as long as it is an
improvement. 


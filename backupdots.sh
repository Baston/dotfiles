#!/bin/bash

function git_check(){
	IS_GIT_AVAILABLE="$(git --version)"
	if [[ $IS_GIT_AVAILABLE == *"version"* ]]; then
		echo " "
	else
		echo "Git is not installed. Fixing it now \n"
		sudo pacman -S git
	fi
}

function backup_dotfiles(){
	cp $HOME/{.bashrc,.bash_profile,.xinitrc,.bash_logout,.profile,.Xauthority,.Xresources} .
	cp -r $HOME/.config/dunst .config/
	cp -r $HOME/.config/i3 .config/
   cp -r $HOME/.config/i3blocks/ .config/
	cp -r $HOME/.config/i3status/ .config/
	cp -r $HOME/.config/neofetch .config/
	cp -r $HOME/.config/nvim/{spell,init.vim} .config/
	cp -r $HOME/.config/scripts .config/
	cp -r $HOME/.config/newsboat/{config,urls} .config/newsboat/
	cp -r $HOME/.config/zathura .config/
	cp -r $HOME/.config/mpv .config/
	cp -r $HOME/.config/polybar .config/
	cp -r $HOME/.config/clipit .config/
	cp -r $HOME/.config/htop .config/
	cp $HOME/.config/.pythonrc .config/
	cp -r $HOME/.config/screenlayout .config/
	cp -r $HOME/.config/ranger .config/
	cp -r $HOME/.config/wallpapers .config/
}

function backup_packages(){
	echo "Want to backup pacman pkgs? [y/n]"
	read PAC
	if [[ $PAC == "y" ]]; then
		pacman -Qqe | grep -ve "yay" -ve "minecraft" -ve "discord" -ve "steam" -ve "wine" -ve "ventoy-bin" > packages.txt
	fi
}

function git_commit(){
	echo "Changes:"
	git add -A
	git commit -a -m "$(date)"
}

function git_push(){
	echo "Do you want to git push? y/n"
	read GIT_PUSH
	if [ $GIT_PUSH == y ]; then
		git push
	fi
}

function backup_things(){
	echo "WARNING!!! This script must be run from WITHIN the repo folder"
   echo "Did you rememeber to convert the images first?"
	echo "Is this the case? YES/n"
	
	read CONTNU
	if [ $CONTNU != YES ]; then
		echo "You din not input YES. The script will now exit"
		exit 1
	fi
	git_check
	backup_dotfiles
	backup_packages
	git_commit
	git_push
}

backup_things

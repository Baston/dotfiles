#!/bin/bash

echo "!!!WARNING!!! Running this script will overwrite ALL dotfiles in .config"
echo "Continue? y/n"
read CONTNU
if [ $CONTNU != y ]; then
	echo "You did not enter YES, goodbye"
	exit 1
fi

# Take use options
echo "Install additional software with pacman? [y/n]"
read INSTALL

echo "Restore dotfiles? [y/n]"
read RESTORE

# Get username
if [[ $RESTORE == y ]]; then
	echo "Which user should get the dotfiles?"
	read USERNAME
fi

# Install software with pacman
if [[ $INSTALL == y ]]; then
	echo "Installing software from list"
	sudo pacman -S - < packages.txt
	echo "You probs want to install yay now..."

	echo "Install yay then install packages? [y/n]"
	read YAYorNAY
	if [[ $YAYorNAY == y ]]; then
		cd ..
		git clone https://aur.archlinux.org/yay.git
		cd yay
		makepkg -si
		cd ../dotfiles
	fi
fi

# Restore dotfiles
if [[ $RESTORE == y ]]; then
	\cp -r .config /home/$USERNAME/
	\cp {.bashrc,.bash_profile,.xinitrc,.bash_logout,.profile,.Xauthority,.Xresources} /home/$USERNAME/
	
fi

